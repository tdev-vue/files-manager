export default {
  choose_file: "Выбрать...",
  size: "Размер",
  upload: "Загрузить",
  preview: "Предосмотр",
  name: "Название",
  edited: "Дата",
  search: "Поиск",
  delete: "Удалить",
  upload_new: "Добавить",
  list: "Список",
  media_library: "Медиа библиотека",
  show: "Показать",
  entities: "элементов",
  total: "Показано с {from} по {to} из {total} (всего {last_page} страниц)",
  drag_drop_files: "Drag & Drop Files",
  files_manager: "Менеджер файлов"
}

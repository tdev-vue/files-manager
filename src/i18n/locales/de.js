export default {
  choose_file: "Wählen...",
  size: "Size",
  upload: "Laden",
  preview: "Vorschau",
  name: "Name",
  edited: "Bearbeitungsdatum",
  delete: "Löschen",
  confirm_delete: "Möchten Sie diesen Artikel wirklich löschen?",
  search: "Suche",
  upload_new: "Neuer Upload",
  list: "Liste",
  media_library: "Medienbibliothek",
  show: "Ergebnisse pro Seite",
  entities: "Entitäten",
  total: "Zeige {from} bis {to} von {total} ({last_page}  Seiten insgesamt)",
  drag_drop_files: "Drag & Drop von Dateien",
  files_manager: "Dateimanager"
}

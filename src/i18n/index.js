import Vue from "vue";
import VueI18n from "vue-i18n";
import ru from "./locales/ru";
import en from "./locales/en";
import de from "./locales/de";

Vue.use(VueI18n);
const lang = document.querySelector('html').getAttribute('lang');
export default new VueI18n({
  locale: lang,
  fallbackLocale: 'en',
  messages: {
    ru: ru,
    en: en,
    de: de
  }
})
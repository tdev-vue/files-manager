export default {
    state: {
        previews: [],
        media: {},
        modal: false,
        eventUniqId: null,
        selected: null
    },
    mutations: {
        ADD_PREVIEW: (state, data) => {
            state.previews.push(data);
        },
        SET_ATTACHMENTS: (state, data) => {
            state.media = data;
        },
        EDIT_MEDIA: (state, data) => {
            const file = state.media.data.findIndex(file => file.id === data.id);
            state.media.data[file].original_name = data.original_name;
        },
        REMOVE_MEDIA: (state, index) => {
            state.media.data.splice(index, 1);
        },
        DROP_PREVIEW: (state, index) => {
            state.previews.splice(index, 1);
        },
        RESET_PREVIEWS: (state) => {
            state.previews = [];
        },
        TRIGGER_MODAL: (state) => {
            state.modal = !state.modal;
        },
        SET_EVENT_UNIQID: (state, uniqId) => {
            state.eventUniqId = typeof uniqId === typeof undefined ? null : uniqId;
        },
        SET_SELECTED: (state, selected) => {
            state.selected = typeof selected === typeof undefined ? null : selected;
        }
    },
    actions: {}
};

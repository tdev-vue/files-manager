import FmList from './components/FmList';
import FmImg from './components/FmImg';
import FmUploader from './components/FmUploader';
import FmModal from './components/FmModal';
import FmButton from './components/FmButton';
import FilesManager from './components/FilesManager';
import VueSilentBox from 'vue-silentbox'
import Vuex from 'vuex'
import $fmStore from './store'
import VueI18n from "vue-i18n";
import "./assets/style.scss";
import axios from './axios';

const Components = {
  FmList,
  FmUploader,
  FmModal,
  FmImg,
  FilesManager,
  FmButton
};

const Tdfm = {
  install(Vue, options) {

    Vue.use(VueSilentBox);
    Vue.use(Vuex);
    Vue.use(VueI18n);
    window.filesManagerStore = Vue.prototype.$fmStore = new Vuex.Store($fmStore);
    window.filesManagerVueHub = Vue.prototype.$filesManagerHub = new Vue();
    Vue.prototype.$http = options.axios ? options.axios : axios;
    Vue.prototype.$filesManagerClasses = (classes) => {
      let newClassesList = [], cssClass;

      if (typeof options.bootstrap !== 'undefined' && !options.bootstrap) {
        for (cssClass of classes) {
          cssClass = 'tdfm-' + cssClass
        }
        newClassesList.push(cssClass);
      } else {
        newClassesList = classes;
      }

      return newClassesList.join(' ')
    };
    Vue.filter('formatBytes', (bytes, decimals) => {
      if (bytes === 0) {
        return '0 Bytes';
      }
      let k = 1024,
        dm = decimals <= 0 ? 0 : decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    });
    Object.keys(Components).forEach(name => {
      Vue.component(name, Components[name])
    })

  }
};

export default Tdfm;

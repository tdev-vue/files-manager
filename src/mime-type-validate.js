/**
 * @param type
 * @return {string|null}
 */
export default function (type) {
  if (type.search(/^audio\//) > -1) {
    return 'audio';
  } else if (type.search(/^image\//) > -1) {
    return 'image';
  } else if (type.search(/^video\//) > -1) {
    return 'video';
  } else if (type === 'application/pdf') {
    return 'pdf';
  } else if (type === 'text/csv') {
    return 'csv';
  }

  return null;
}
# Files manager.

VUE.js components and CkEditor plugin for [Laravel Files Manager](https://gitlab.trush-dev.com/laravel/files-manager)

# CkEditor

Copy directory `filesmanager` from `ckeditor` to your `<ckeditor_root>/plugins`

Add to CkEditor config:

1. `config.allowedContent = true;`
1. `config.extraAllowedContent = 'img[!src,!class],picture[!class],source[!srcset,!type]';`
1. `config.extraPlugins = 'filesmanager';`
1. `config.removePlugins = 'image';`

# Author

[Andrii Trush]()

# Licence

Copyright &copy; 2019, [TDev](https://trush-dev.com). You may not make any copies without the permission of the author.


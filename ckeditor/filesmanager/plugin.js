(function () {
  CKEDITOR.plugins.add('filesmanager', {
    icons: 'filesmanager',
    init: function (editor) {
      editor.addCommand('filesManagerCommand', {
        exec: function (editor) {
          var uniqueId = Math.random().toString(36).slice(2);
          filesManagerStore.commit('SET_EVENT_UNIQID', uniqueId);
          filesManagerStore.commit('TRIGGER_MODAL', true);
          filesManagerVueHub.$on(uniqueId, function (id) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', filesManagerActions.show + '?id=' + id, true);
            xhr.onload = function () {
              try {
                var div = document.createElement('div'),
                  picture = document.createElement('picture'),
                  source,
                  key,
                  img,
                  res = JSON.parse(xhr.response);
                if (typeof res.sources === 'object' && Object.keys(res.sources).length > 0) {
                  for (key in res.sources) {
                    source = document.createElement('source');
                    source.setAttribute('srcset', res.sources[key].src);
                    source.setAttribute('type', res.sources[key].mime);
                    picture.appendChild(source)
                  }
                }
                img = document.createElement('img');
                img.setAttribute('src', res.src);
                img.setAttribute('alt', '');
                img.setAttribute('class', 'img-fluid img-responsive');
                picture.appendChild(img);
                picture.setAttribute('class', 'fm-picture');
                div.setAttribute('class', 'fm-picture-container');
                div.appendChild(picture);
                editor.insertHtml(div.outerHTML);
              } catch (e) {
                alert(e.message);
              }
            };
            xhr.send()
          });
        }
      });
      editor.ui.addButton('FilesManager', {
        label: 'Files Manager',
        command: 'filesManagerCommand'
      });
    },
  });

})();
